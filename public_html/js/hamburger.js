/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function () {
    // open het menu
    jQuery("#hamburger").click(function () {
        jQuery('#content').css('min-height', jQuery(window).height());
        
        /* geeft het menu weer */
        jQuery('nav').css('opacity', 1);        // Verander de opacity van het nav element, van zodra op de hamburger wordt geklikt
        
        /* bij het klik event wordt met de jquery de inhoud van de pagina opgevraagd en wordt de 
         * breedte van het content element vastgezet. Hiermee worden onze originele proporties
         * van het scherm opgeslagen (we moeten die bijhouden in een variabele). Wanneer de inhoud
         * uit onze viewport naar rechts wordt geschoven. => breedte van de content container niet scalen terwijl dat we een
         * animatie uitvoeren. */
        
        var contentWidth= jQuery('#content').width(); // oorpronkelijke, originele breedte bijhouden (afhankelijk vh scherm waarmee je surft)
        
        jQuery('#content').css('width', contentWidth);
        
        /* Hieronder gaan we een laag weergeven dat het klikken en het scrollen zal disablen
         * wanneer het menu'tje wordt getoond */
        jQuery('#contentlayer').css('display', 'block');
        
        jQuery('#container').bind('touchmove', function(e) {
            e.preventDefault()
        });
        
        /* de margin gaan vastzetten voor de volledige container (die we nodig hebben om de volledige animatie te doen) */
        jQuery("#container").animate({"marginLeft": ["70%", 'easeOutExpo']},{duration: 700});
        
    });
    
    // menu sluiten
    jQuery("#contentlayer").click(function(){
        // Toestaan van alle scrolling op mobiele devices wanneer het menu weer wordt gesloten
        jQuery('#container').unbind('touchmove');
        
        // de margins weer in originele staat zetten met een animation.
        jQuery("#container").animate({"marginLeft": ["-1",'easeOutExpo']},
        {duration:700,
            complete: function()
            {
                        jQuery('#content').css('width', 'auto');
                        jQuery('#contentlayer').css('display', 'none');
                        jQuery('nav').css('opacity',0);
                        jQuery('#content').css('min-height', 'auto');
            }
        });
    });
    
});