/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* zo begin je altijd: */
$(document).ready(function(){
    var slider_nav = $("#slideshow-nav");
    slider_nav.find('a[href="#slide1"]').addClass("active");   /* a tag aanspreken binnen deze div */
    
    slider_nav.localScroll({
        target: '#slideshow',  /* in deze container moet hij dit gaan weergeven */
        axis: 'x',              /* op welke as moet die schuiven? */
        duration: 700
    });
    
    /* in plaats van click zou je eventueel een timer functie ook kunnen gebruiken... */
    slider_nav.find("a").click(function(){    /* van zodra je o een a element op klikt, moet je 'iets' doen...*/
        slider_nav.find("a").removeClass("active");
        $(this).addClass("active");
    });
    
});